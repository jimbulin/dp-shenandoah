#!/bin/sh
cd /home/test

JDK=${1:-jdk9}
JDK_DIR=`pwd`/$JDK

# ceska casova zona pro lepsi orientaci
unlink /etc/localtime
ln -s /usr/share/zoneinfo/Europe/Prague /etc/localtime

ulimit -n 4096

echo "install required packages dependencies"
yum -y install java-1.7.0-openjdk-devel java-1.8.0-openjdk-devel libXtst-devel libXt-devel libXrender-devel libXi-devel cups-devel freetype-devel alsa-lib-devel hg cpuid numactl autofs
yum -y -q install http://dl.fedoraproject.org/pub/epel/7/x86_64/h/htop-2.0.2-1.el7.x86_64.rpm

echo "turning off the swap"
swapoff -a


echo "Clone the repository"
hg clone http://hg.openjdk.java.net/shenandoah/$JDK/ $JDK_DIR
echo "Update the repository"
pushd $JDK_DIR
sh get_source.sh 
popd