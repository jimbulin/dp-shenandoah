#!/bin/bash
. /root/.bashrc

export HEAP_SIZE=${1:-100g}
setjava /home/test/jdk8u/build/linux-x86_64-normal-server-release/images/j2sdk-image
export JAVA_OPTS="-Xms$HEAP_SIZE -Xmx$HEAP_SIZE -XX:+UseShenandoahGC -XX:+ShenandoahLogConfig -XX:+PrintGCDetails -XX:+PrintGCTimeStamps";

export SPECJVM_HOME=/home/test/SPECjvm2008
export LOGS_HOME=/home/test/spec-results

if [ ! -d $LOGS_HOME ]; then mkdir -p $LOGS_HOME; fi;

TESTS=( compiler.compiler compiler.sunflow compress crypto.aes crypto.rsa crypto.signverify derby mpegaudio scimark.fft.large scimark.lu.large scimark.sor.large scimark.sparse.large scimark.fft.small scimark.lu.small scimark.sor.small scimark.sparse.small scimark.monte_carlo serial sunflow xml.transform xml.validation  )

pushd $SPECJVM_HOME

for TEST in "${TESTS[@]}"; do

./run-specjvm.sh $TEST | tee $LOGS_HOME/$HEAP_SIZE_$TEST.log

done




popd
