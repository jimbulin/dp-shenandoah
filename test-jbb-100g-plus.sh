#!/bin/bash

for HEAP_SIZE in  "400g" #"100g" ; 
do
	for CPUS in 24 8 #40 #24 8; 
	do
		for HEURISTICS in "dynamic" "newadaptive" "lazy";
		do

#if [ "$HEURISTICS" = "dynamic" ] && [ "$HEAP_SIZE" = "400g" ]; then
 # continue;
#fi

if [ "$1" = "--dry-run" ]; then
echo $HEAP_SIZE $CPUS $HEURISTICS
else
./test-jbb.sh $HEAP_SIZE $CPUS $HEURISTICS
fi

done;
done;
done;
