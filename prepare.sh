#!/bin/sh
JDK_DIR=${1:-jdk9}

pushd $JDK_DIR

#get sources
echo "Clone the repository"
hg clone http://hg.openjdk.java.net/shenandoah/$JDK_DIR/ .
echo "Update the repository"
sh get_source.sh 


#get dependencies
echo "install required packages dependencies"
sudo dnf -y install libXtst-devel libXt-devel libXrender-devel libXi-devel cups-devel freetype-devel alsa-lib-devel

popd
