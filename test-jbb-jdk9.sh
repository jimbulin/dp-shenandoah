#!/bin/bash
set -x
export BASE_DIR=/home/test/dp
export JAVA_HOME=$BASE_DIR/jdk9-shenandoah-latest/build/linux-x86_64-normal-server-release/images/jdk
export PATH=$JAVA_HOME/bin:$PATH


export SPECJBB_HOME=$BASE_DIR/SPECjbb2015_1.00
export LOGS_HOME=$BASE_DIR/specjbb-gclogs
export TIMESTAMP=$(date '+%y-%m-%d_%H%M%S')


export HEAP_SIZE=${1:-50g}
export CPUS=${2:-40}
export HEURISTICS=${3:-dynamic}

export RESULT_DIR=$LOGS_HOME/result-${HEAP_SIZE}-${CPUS}threads-${HEURISTICS}-${TIMESTAMP}
if [ ! -d $RESULT_DIR ]; then mkdir -p $RESULT_DIR; fi;
export GC_LOG=$LOGS_HOME/gclog-${HEAP_SIZE}-${CPUS}threads-${HEURISTICS}-${TIMESTAMP}.log

export JAVA_OPTS="--add-modules java.xml.bind -XX:+UseShenandoahGC -Xms$HEAP_SIZE -Xmx$HEAP_SIZE -XX:ShenandoahGCHeuristics=$HEURISTICS -XX:+PrintGCDetails -Xloggc:$GC_LOG";


## below SPECjbb specific options with modified working dir
SPEC_OPTS=""
# Optional arguments for the Composite mode (-l <num>, -p <file>, -skipReport, etc.)
MODE_ARGS=""

# Copy current config to the result directory
cp -r $SPECJBB_HOME/config $RESULT_DIR

pushd $RESULT_DIR

echo "Run: $TIMESTAMP"
echo "Launching SPECjbb2015 in Composite mode..."
echo

echo "Start Composite JVM"
java $JAVA_OPTS $SPEC_OPTS -jar $SPECJBB_HOME/specjbb2015.jar -m COMPOSITE $MODE_ARGS 2>composite.log | tee composite.out &
  
COMPOSITE_PID=$!
echo "Composite JVM PID = $COMPOSITE_PID"

sleep 3

echo
echo "SPECjbb2015 is running..."
echo "Please monitor $result/controller.out for progress"

wait $COMPOSITE_PID
echo
echo "Composite JVM has stopped"

echo "SPECjbb2015 has finished"
echo

popd
set +x



