#!/bin/sh
JDK_DIR=${1:-jdk9}
LOG=`pwd`/log-build-$(basename $JDK_DIR)-$(date +%s).log

pushd $JDK_DIR

#ensure java environment
export JAVA_HOME=/opt/jdk/oracle/jdk1.8.0_last
export PATH=$JAVA_HOME/bin:$PATH
unset JAVA_HOME

#configure
bash ./configure | tee -a $LOG

#clean
#make clean | tee -a $LOG
#build images
make clean images | tee -a $LOG

popd
