set -x
#get max logical cpu
MAX_CPUS=$(find /sys/devices/system/cpu/cpu[0-9]* -maxdepth 0 | wc -l)

MODE="--balanced"
case $1 in
"--prefer-real-core")
    MODE="--prefer-real-core"
    shift
    ;;
"--balanced")
    MODE="--balanced"
    shift
    ;;
*)
    ;;
esac


NUMBER_CPUS=${1:-$MAX_CPUS} # 1 <= number <= maxcpu
if (( $NUMBER_CPUS < 1 )); then exit 1; fi
if (( $NUMBER_CPUS > $MAX_CPUS )); then NUMBER_CPUS=$MAX_CPUS; fi



#enable all
for CPU in /sys/devices/system/cpu/cpu[0-9]*; do 
echo "enabling $CPU "
if [ -f $CPU/online ] && [ $(cat $CPU/online) == "0" ]; then
  echo 1 > $CPU/online
fi
done


N_CPU_TO_DISABLE=$(($MAX_CPUS - $NUMBER_CPUS))

if (( $N_CPU_TO_DISABLE == 0 )); then exit 0; fi

BALANCED_LIST=$(mktemp)
HT_SORTED_LIST=$(mktemp)

# NOTE: you should remove by multiply of two to quarantee both HT threads are removed
#sorted by core, socket, HT threads on subsequent lines
egrep "(( id|processor).*:|^ *$)" /proc/cpuinfo| tr "\n" "#" | sed "s/##/\\n/g"| sed "s/\\t\|\ //g" | tr "#" ":" | sort -t: -k6n -k4n -k2n > $BALANCED_LIST

cat $BALANCED_LIST | sed -n "p;n" > $HT_SORTED_LIST
cat $BALANCED_LIST | sed -n "n;p" >> $HT_SORTED_LIST

case $MODE in
"--prefer-real-core")
    TO_DISABLE=$(cat $HT_SORTED_LIST| tail -n $N_CPU_TO_DISABLE | cut -d: -f2)
    ;;
*)
    TO_DISABLE=$(cat $BALANCED_LIST| tail -n $N_CPU_TO_DISABLE | cut -d: -f2)
    ;;
esac


#disable desired
for CPU in ${TO_DISABLE[@]}; do 
 echo "disabling /sys/devices/system/cpu/cpu$CPU "
 echo 0 > /sys/devices/system/cpu/cpu$CPU/online
done
set +x
